# Angular 7 demo project for git submodule

```sh
  git clone https://bitbucket.org/crosstechit/ng-demo --recurse-submodules
  cd ng-demo
  npm install # or yarn
```

To add new angular module to library

```sh
  NAME=module_name npm run library:add
```

To update library to latest commit

```sh
  npm run library:pull
```

To push local changes to repository

```sh
  npm run library:push
```
